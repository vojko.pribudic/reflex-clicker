# ReflexClicker image detection bot



## About

Code repository for the [article I wrote for Medium](https://vojko-pribudic.medium.com/create-image-recognition-bot-with-python-8524dfce4b95)

Check the [resulting video](https://www.youtube.com/watch?v=7II1EdYZvE4)